### Требования:

 - Установленный ```python3```

 - Установленный ```nodejs```

### Для развёртки проекта:

 - Перейти в папку репозитория ```$ cd  ```
 
 - Установить виртальную среду (если не установлена) ```$ pip install virtualenv```
 
 - Создать среду ```$ virtualenv -p python3 env``` или ```$ virtualenv env```
 
 - Активировать среду ```$ source env/bin/activate``` или ```$ env/Scripts/activate.ps1```
  
 - Установить зависимости python ```$ pip install -r local_requirements.txt```
 
 - Установить зависимости js ```$ npm install```

 - Собрать зависимости js ```$ npm run-script build```
 
 - Запустить сервер ```$ python manage.py runserver```