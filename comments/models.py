from django.db import models
from sight.models import Sight

MARKS = [
    (1,'Awful'),
    (2,'Bad'),
    (3,'Good'),
    (4,'Very good'),
    (5,'Excellent')
]

class Comment(models.Model):
    name = models.CharField(max_length=50)
    text = models.TextField()
    rating = models.IntegerField(choices=MARKS)
    sight = models.ForeignKey(Sight, on_delete=models.CASCADE, related_name='comments')

    date_created = models.DateField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name + ' ' + self.sight.name