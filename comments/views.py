from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Comment
from .serializers import CommentSerializer

class CommentListAPIView(ListCreateAPIView):
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.filter(sight__slug=self.kwargs.get('slug'))

class CommentDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = CommentSerializer
    lookup_field = 'id'
    def get_queryset(self):
        return Comment.objects.filter(sight__slug=self.kwargs.get('slug'))