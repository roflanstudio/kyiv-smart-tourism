from django.apps import AppConfig


class FrontendServeConfig(AppConfig):
    name = 'frontend_serve'
