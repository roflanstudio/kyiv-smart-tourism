import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';


import './app.sass';

import Header from '../header';
import Main from '../main';
import DetailsView from '../details-view';
import QrCode from '../qr-code';

class App extends React.Component { 	

    render() {
        return (
			<Fragment>
				<Router>
					<Header/>
					<Route path='/' exact component={() => <Main/>} />
					<Route path='/:id' component={DetailsView} />
					<Route path='/:id/qr' component={QrCode} />
				</Router>
			</Fragment>
        );
      };
};

export default App;