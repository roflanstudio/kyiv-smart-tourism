import React, {Component} from 'react';

import './card.sass';

import StarRatings from 'react-star-ratings';

class Card extends Component {

    render_stars = (rating) => {
        if (rating !== null){
            return <StarRatings
                rating={rating}
                starRatedColor="blue"
                numberOfStars={5}
                starDimension="30px"
                starSpacing="6px"
                />
        }
    }

    render(){
        const {link, img, name, desc, rate} = this.props;
        return (
            <div className="card">
                <a href={link}>
                    <div className="wrap">
                        <div className="img-wrap">
                            <div>
                                <img src={img}/>
                            </div>
                        </div>
                        <div className="card-content">
                            <div className="card-name">
                                <h2>{name}</h2>
                            </div>
                            <div className="card-desc">
                                {desc}
                            </div>
                            <div className="star-rating">
                            {this.render_stars(rate)}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        );
    }
};

export default Card;