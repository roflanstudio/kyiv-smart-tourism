import React from 'react';

import Comment from '../comment';

import './comment-list.sass'

const CommentList = ({comments}) => {
    console.log(comments);
    const commentsArr = comments.map((comment) => <Comment key={comment.name} name={comment.name} text={comment.text} rating={comment.rating}/>);
    return (
        <div className="comments-arr">
            {commentsArr}
        </div>
    )
}

export default CommentList;