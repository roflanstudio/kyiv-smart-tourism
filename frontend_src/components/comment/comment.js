import React from 'react';

import './comment.sass';

import StarRatings from 'react-star-ratings';

const Comment = ({name, text, rating}) => {
    
    return (
        <div className="comment">
            <div className="comment-name">
                Name: {name}
            </div>
            <div className="comment-text">
                Comment: {text}
            </div>
            <div className="comment-rating">
                <StarRatings
                    rating={rating}
                    starRatedColor="blue"
                    numberOfStars={5}
                    starDimension="30px"
                    starSpacing="6px"
                    />
            </div>
        </div>
    )
}

export default Comment;