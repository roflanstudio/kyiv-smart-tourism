import React, {Component, useState} from 'react';
import {Link} from 'react-router-dom';
import ApiService from '../../services/api_service';
const { getSight, getComments } = new ApiService();
import ReactMapGL from 'react-map-gl';

import CommentList from '../comment-list';

import './details-view.sass'

class DetailsView extends Component {
    constructor(props){
        super(props);
        const {id} = this.props.match.params;
        this.state = {
            data: null,
            slug: id,
            viewport: {
                width: 400,
                height: 400,
                latitude: 0,
                longitude: 0,
                zoom: 15
            }
        }
    }
    
    componentDidMount = async () =>  {
        const data = await getSight(this.state.slug);
        this.setState({
            data: data,
            viewport: {
                width: 400,
                height: 400,
                latitude: data.lat,
                longitude: data.long,
                zoom: 15
            }});
    }
    // render_stars = (rating) => {
    //     if (rating !== null){
    //         return <StarRatings
    //             rating={rating}
    //             starRatedColor="blue"
    //             numberOfStars={5}
    //             starDimension="30px"
    //             starSpacing="6px"/>
    //     }
    // }


    render() {
        

        if (this.state.data !== null){
            const {pic, name, desc, rate} = this.state.data;
            const path = `/${this.state.slug}/qr/`;
            return (
                <div>
                    <div className="details-wrap">
                        
                        <div className="card-content">
                            <div className="card-name">
                                <h2>{name}</h2>
                            </div>
                            <div className="card-desc">
                                {desc}
                            </div>
                            {/* {this.render_stars(rate)} */}
                        </div>
                    </div>
                    <div className="map-wrap">
                        <div className="img-wrap">
                            <img src={pic}/>
                        </div>
                        <div className="map">
                            <div>
                                <ReactMapGL
                                    {...this.state.viewport}
                                    mapboxApiAccessToken={'pk.eyJ1IjoiZGVudGhlZ3JlYXQiLCJhIjoiY2s2eWVtb3RzMHVuMzNsbXZxbDA2Zm8zNSJ9.x9mYXd_g9iEcyO48ahdq8Q'}
                                    // onViewportChange={setViewport}
                                    />
                            </div>
                        </div>
                        
                    </div>

                    <div className="details-comments">
                        <CommentList comments={this.state.data.comments}/>
                    </div>

                    <div className="get-qr">
                        <Link to={path}>Get QR code</Link>
                    </div>
                </div>
            )
        }
        else return null
    }
}

export default DetailsView;