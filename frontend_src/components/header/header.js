import React, {Fragment} from 'react';

import {Link} from 'react-router-dom';

import logo from '../../img/logo.png';

import './header.sass';

const Header = () => {
    return(
        <header>
            <div>
                <Link to='/'><img src={logo} className='logo'/></Link>
            </div>
        </header>
    );
};

export default Header;