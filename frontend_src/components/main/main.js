import React, {Fragment} from 'react';
import ApiService from '../../services/api_service';
import './main.sass';

import Card from '../card';

const { getAllSights } = new ApiService();

class Main extends React.Component{

    state = {
        data: null
    }

    componentDidMount = async () =>  {
        this.setState({data: await getAllSights()})
    }

    render_cards = () => {
        if (this.state.data !== null)
        {
            let cards = [];
            for (const sight of this.state.data) {
                cards.push(
                    <Card
                        key= {sight.name} 
                        img= {sight.pic}
                        link={sight.slug}
                        desc={sight.desc}
                        rate={sight.rate}
                        name={sight.name}/>
                )
            }
            return cards
        }
        else return []
    }

    render = () => {
        return (
            <Fragment>
                <div className="content">
                    <h1>
                        Welcome to our travel site!
                    </h1>
                    <div className="content-text">
                        Kyiv is the capital and most populous city of Ukraine. It is in north-central Ukraine along the Dnieper River. Its population in July 2015 was 2,887,974, making Kiev the 6th-most populous city in Europe.
                        Kiev is an important industrial, scientific, educational and cultural center of Eastern Europe. It is home to many high-tech industries, higher education institutions, and historical landmarks. The city has an extensive system of public transport and infrastructure, including the Kiev Metro.
                        The city's name is said to derive from the name of Kyi, one of its four legendary founders.                </div>
                </div>
                <div className="catalog">
                    {this.render_cards()}        
                </div>
            </Fragment>
        );
    };
}

export default Main;