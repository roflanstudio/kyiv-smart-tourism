import React, {Component} from 'react';
import ApiService from '../../services/api_service';
const { getQr } = new ApiService();

import './qr-code.sass';
class QrCode extends Component {

    constructor(props){
        super(props);
        console.log("huy");
        const {id} = this.props.match.params;

        this.state = {
            data: null,
            slug: id
        }
    }
    
    componentDidMount = async () =>  {
        this.setState({data: await getQr(this.state.slug)})
        console.log(this.state.data);
    }
    render() {
        if (this.state.data !== null){
            return (
                <div className="qr">
                    <img src={this.state.data}/>
                </div>
            )
        } else return null
    }
}

export default QrCode;