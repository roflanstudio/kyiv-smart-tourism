export default class ApiService {

    apiBase = '/api';
  
    getResource = async (url) => {
      const res = await fetch(`${this.apiBase}${url}`);
  
      if (!res.ok) {
        throw new Error(`Could not fetch ${url}` +
          `, received ${res.status}`)
      }
      return await res.json();
    };
  
    getAllSights = async () => {
      const res = await this.getResource(`/sights/`);
      console.log(res);
      return res
        .map(this._transformSight);
    };
    
	getSight = async (id) => {
		const res = await this.getResource(`/sights/${id}/`);
		return this._transformOneSight(res);
	};
	
	getQr = async (id) => {
		const res = await this.getResource(`/sights/${id}/qr/`);
		return res;
  }

    
    _extractId = (item) => {
      const idRegExp = /\/([0-9]*)\/$/;
      return item.url.match(idRegExp)[1];
    };
  
    _transformSight = (sight) => {
      return {
          slug: sight.slug,
          name: sight.name,
          pic: sight.pic,
          desc: sight.short_description,
          rate: sight.avg_rating
      };
	};
	_transformOneSight = (sight) => {
		return {
			slug: sight.slug,
			name: sight.name,
			pic: sight.pic,
			desc: sight.description,
			rate: sight.avg_rating,
			categories: sight.categories,
			comments: sight.comments,
			lat: sight.lat,
			long: sight.long

		}
	}

  
  }