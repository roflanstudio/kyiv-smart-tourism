from .settings import *

# DEBUG = False

SECRET_KEY = os.environ['SECRET_KEY']

# SECURE_SSL_REDIRECT = True
# SESSION_COOKIE_SECURE = True
# SECURE_BROWSER_XSS_FILTER = True
# SECURE_CONTENT_TYPE_NOSNIFF = True
# SECURE_HSTS_SECONDS = 30
# SECURE_HSTS_INCLUDE_SUBDOMAIN = True
# SECURE_HSTS_PRELOAD = True
# CSRF_COOKIE_SECURE = True
# X_FRAME_OPTIONS = 'DENY'

import django_heroku
django_heroku.settings(locals())