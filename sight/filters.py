from django_filters import rest_framework as filters
from .models import Sight
from category.models import Category


class SightFilter(filters.FilterSet):
    categories = filters.ModelMultipleChoiceFilter(queryset=Category.objects.all())

    class Meta:
        model = Sight
        fields = [
                  'categories',
                 ]