from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from category.models import Category

from .utils import generate_slug

class Sight(models.Model):
    name = models.CharField(max_length=80)
    pic = models.ImageField()
    description = models.TextField(blank=True,null=True)
    is_approved = models.BooleanField(default=True)
    slug = models.SlugField(unique=True,blank=True)
    lat = models.FloatField(default=50.403912)
    long = models.FloatField(default=30.686163)
    categories = models.ManyToManyField(Category, related_name='sights', null=True)

    def __str__(self):
        return self.name

@receiver(pre_save,  sender = Sight)
def sight_pre_save(sender, instance, *args, **kwargs):
    if Sight.objects.filter(id=instance.id).exists():
        if Sight.objects.get(id=instance.id).name != instance.name:
            instance.slug = generate_slug(str(instance), instance)
    else:
        instance.slug = generate_slug(str(instance), instance)