from rest_framework import serializers
from category.serializers import CategorySerializer
from comments.serializers import CommentSerializer
from .models import Sight

class SightListSerializer(serializers.ModelSerializer):

    MAX_DESCRIPTION_LEN = 350

    short_description = serializers.SerializerMethodField()
    avg_rating = serializers.SerializerMethodField()
    class Meta:
        model = Sight
        fields = [
            'slug',
            'name',
            'pic',
            'short_description',
            'avg_rating',
            # 'categories'
        ]

    def get_short_description(self, object):
        if object.description != None:
            return object.description[:self.MAX_DESCRIPTION_LEN] + '...' if len(object.description) >= self.MAX_DESCRIPTION_LEN else object.description
        else:
            return None

    def get_avg_rating(self, object):
        ratings = [i.rating for i in object.comments.all()]
        return sum(ratings)/len(ratings) if len(ratings) > 0 else None

class SightDetailSerializer(serializers.ModelSerializer):
    avg_rating = serializers.SerializerMethodField()
    categories = CategorySerializer(many=True)
    comments = CommentSerializer(many=True)
    class Meta:
        model = Sight
        fields = '__all__'

    def get_avg_rating(self, object):
        ratings = [i.rating for i in object.comments.all()]
        return sum(ratings)/len(ratings) if len(ratings) > 0 else None