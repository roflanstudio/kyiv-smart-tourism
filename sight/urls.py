from django.urls import path, re_path 
from .views import SightListAPIView, SightDetailAPIView, SightQRView
from comments.views import CommentListAPIView, CommentDetailAPIView

urlpatterns = [
    path('', SightListAPIView.as_view()),
    path('<slug:slug>/', SightDetailAPIView.as_view()),
    path('<slug:slug>/qr/', SightQRView.as_view()),
    path('<slug:slug>/comments/', CommentListAPIView.as_view()),
    path('<slug:slug>/comments/<int:id>/', CommentDetailAPIView.as_view()),
]