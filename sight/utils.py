from transliterate import translit
import random, string

def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def generate_slug(text:str,instance):
    result = translit(text.lower(),reversed=True,language_code='ru').replace(' ','-')
    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=result).exists()
    if qs_exists:
        result += random_string_generator(size=6)
    return result