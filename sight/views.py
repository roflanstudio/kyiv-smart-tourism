from rest_framework.generics import (RetrieveUpdateDestroyAPIView, 
                                    RetrieveAPIView, 
                                    ListCreateAPIView,
                                    ListAPIView)
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import get_object_or_404
import qrcode
import base64
from io import BytesIO
from .filters import SightFilter
from .models import Sight
from .serializers import SightListSerializer, SightDetailSerializer

class SightQRView(APIView):
    def get(self, request, **kwargs):
        obj = get_object_or_404(Sight,is_approved=True,slug=kwargs.get('slug'))
        img = qrcode.make(request.build_absolute_uri('/')[:-1]+'/'+obj.slug+'/qr').get_image()
        buffered = BytesIO()
        img.save(buffered, format="png")
        img_str = base64.b64encode(buffered.getvalue())
        return Response(b'data:image/png;base64,'+ img_str )

class SightListAPIView(ListAPIView):
    '''
    Список достопримечательностей
    '''
    queryset = Sight.objects.filter(is_approved=True)
    serializer_class = SightListSerializer
    filter_backends = [DjangoFilterBackend, ]
    filterset_class = SightFilter

class SightDetailAPIView(RetrieveAPIView):
    '''
    Детальное отображение достопримечательностей
    '''
    queryset = Sight.objects.filter(is_approved=True)
    serializer_class = SightDetailSerializer
    lookup_field = 'slug'
