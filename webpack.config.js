var path = require("path");
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');
// var CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
    context: __dirname,

    entry: 
    {
        'polyfill': 'babel-polyfill',
        'main': './frontend_src/index',
    },
    
    output: {
        path: path.resolve('./frontend_serve/static/bundles/'),
        filename: "[name]-[hash].js",
    },

    plugins: [
        // new CaseSensitivePathsPlugin(),
        new BundleTracker({ filename: './webpack-stats.json' }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: [ /\.sass$/, /\.scss$/ ],
                use: ['style-loader', 'css-loader', 'sass-loader']
              },
              {
                test: /\.(gif|png|jpe?g|svg|webp)$/i,
                use: [
                    'url-loader',
                ],
              }
        ]
    },
    resolve: {
        extensions: ['*', '.js', '.jsx']
    }
};